#! /usr/bin/env python3

import cdds as dds

class PingPongBase:
    def __init__(self, val=None):
        self.val = val

    def gen_key(self):
        return type(self).__name__

    def __str__(self):
        return "<%s key='%s': %s>" % (type(self).__name__, self.gen_key(), self.val)

class Ping(PingPongBase): pass # key == Ping
class Pong(PingPongBase): pass # Key == Pong

