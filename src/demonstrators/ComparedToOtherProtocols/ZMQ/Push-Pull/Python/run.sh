#!/bin/bash

Pi0="pi@192.168.0.200"
Pi1="pi@192.168.0.201"
Pi3="pi@192.168.0.203"


# copy MQTT.py the raspberries 
scp ../MQTT.py ${Pi0}:~/MQTT.py
scp ../MQTT.py ${Pi1}:~/MQTT.py
scp	../MQTT.py ${Pi3}:~/MQTT.py



python resultconsumer.py &
python consumer.py	&
python consumer.py	&
python producer.py

sh cleanup.sh