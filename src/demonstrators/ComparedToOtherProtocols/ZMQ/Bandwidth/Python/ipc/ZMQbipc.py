#!/usr/bin/env python3
# (C) ALbert Mietus, dropjes-licentie: copy & use, and betaal met dropjes naar nuttigheid

import zmq

import sys
from time import sleep
from datetime import datetime

import logging
logging.basicConfig(level=logging.INFO)
from logging import getLogger
logger = getLogger(__name__)

ReceiveUrl=        sys.argv[1]      if len(sys.argv) > 1 else 8881 # take the receive port from command line input or use the default one
SendUrl=            sys.argv[2]      if len(sys.argv) > 2 else 8882
IsMaster=           int(sys.argv[3]) if len(sys.argv) > 3 else 0

if IsMaster:
    logger.info("master started")
else:
    logger.info("hub started")
    
NumberOfMessagesToSend = 4000
WhichMessagesReceived = [0]*NumberOfMessagesToSend

ReportEveryXMessages=500

def report(c, t0):
    t1 = datetime.now()
    TotalTime = (t1-t0).total_seconds()
    logger.info("it took %s amount of messages %s seconds which is %s ms/msg" % (c,TotalTime,TotalTime/c*1000 ))
    
        
def send_message_bunch(sender, NumberOfMessages):
    for i in range(NumberOfMessages):
        sender.send_string(str(i))
    pass

def main():

    context = zmq.Context()
    sender = context.socket(zmq.PAIR)
    logger.info("Sending on %s" % SendUrl)
    sender.connect(SendUrl)
    
    context = zmq.Context()
    logger.info("Listening on %s" % ReceiveUrl)
    receiver = context.socket(zmq.PAIR)
    receiver.bind(ReceiveUrl)
    # receiver.connect("tcp://localhost:%s" % ReceievePort)
    
    if IsMaster:
        logger.info("sending first message")
        count = 1
        t0 = datetime.now()
        send_message_bunch(sender, NumberOfMessagesToSend)
        # sender.send_string("Echo %i" % count)
        
        
        while 0 in WhichMessagesReceived:
            msg = int(receiver.recv().decode())
            WhichMessagesReceived[msg] = 1
            
            if WhichMessagesReceived.count(1) % ReportEveryXMessages == 0:
                report(WhichMessagesReceived.count(1), t0)
        print("all messages received")

        
    else:
        logger.info("listening to messages")
        while True:
            msg = receiver.recv().decode()
            sender.send_string(msg)
        pass




if __name__ == "__main__":
    main()