#! /usr/bin/env python3

import sys, time, os
import cdds as dds
from _aux import EVERY, process_from, send_data, demo_out_in, M_IN, M_OUT
from datetime import datetime

ReceivingTopic = sys.argv[1] if len(sys.argv) >1 else M_OUT
SendingTopic   = sys.argv[2] if len(sys.argv) >2 else M_IN

LOOP_SLEEP = 6

def plus_1(v):
    global sender
    send_data(sender, v+1)

def on_receive_data(listner):
    msg,c = process_from(listner, plus_1)
    demo_samples(msg,c);

def report(count, t0):
    t1 = datetime.now()
    dt = (t1-t0).total_seconds()
    print("Processed %i messages, in %2.1f seconds: %i msg/sec [%3.3f ms/msg]" % (
        count, dt,
        count/dt,
        0.0 if count==0 else dt*1000/count))

count=0
pid=os.getpid()
def demo_samples(v,c):
    global count
    if (count % EVERY) < c:
        print("HUB{%s}: sample[%i]: %s" % (pid, count, v))
    count+=c

if __name__ == '__main__':
    global sender, val
    val=None

    rt = dds.Runtime()
    dp = dds.Participant(0)

    t_out = dds.FlexyTopic(dp,  SendingTopic)
    t_in  = dds.FlexyTopic(dp,  ReceivingTopic)
    sender   = dds.FlexyWriter(dp, t_out, [dds.Reliable(), dds.KeepLastHistory(10)])
    receiver = dds.FlexyReader(dp, t_in, on_receive_data, [dds.Reliable(), dds.KeepLastHistory(10)])

    demo_out_in(SendingTopic, ReceivingTopic)
    print("start listening")

    t0 = datetime.now()
    last_count=0
    while True:
        time.sleep(LOOP_SLEEP)
        report(count-last_count, t0)
        last_count=count
        t0 = datetime.now()
