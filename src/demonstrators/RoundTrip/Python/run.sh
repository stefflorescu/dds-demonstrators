#!/bin/bash
# roundtrip isc with DDS

trap ctrl_c INT

ctrl_c() {
		echo "knock knock, cleaning lady"
       	# sh cleanup.sh
}



Pi0="pi@192.168.0.200"
Pi1="pi@192.168.0.201"
Pi3="pi@192.168.0.203"

# copy DDShub.py the raspberries 
scp -r ../RoundTime		${Pi0}:~/RoundTime/
scp -r ../RoundTime		${Pi1}:~/RoundTime/
scp -r ../RoundTime		${Pi3}:~/RoundTime/

echo "running programs "
ssh ${Pi0} python3 ~/RoundTime/DDShub.py hub master	&
ssh ${Pi1} python3 ~/RoundTime/DDSmaster.py master hub

# sh cleanup.sh