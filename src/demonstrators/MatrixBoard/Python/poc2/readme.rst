Startup matrix board
====================

To startup the matrix board the sending topic needs to be provided.
./MatrixBoard.py <sending topic> in different terminals.

e.g. ./MatrixBoard.py 1
     ./MatrixBoard.py 2

The first one will send its generated traffic to topic mb1 and is listening on to other topics for their traffic.
The second one will do the same for topic mb2 and so on. Each sender has two preferred readers. As long as these
preferred readers are not sending, others will be read. In case a preferred reader is matched another, not preferred,
reader will be removed from the list and the preferred one is added to the list of readers.
So the first one will receive generated traffic from the second one and adapt the speed on the matrix board.
