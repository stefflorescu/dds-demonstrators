.. (C) ALbert Mietus, Sogeti

=============
DDS, an intro
=============

DDS is a real-time, embedded *“data-centric"* **pub-sub** communication framework. It has a long history and is very well described. Although many documentation shows the area when it is described (a bit old-school). However, it is very alive!

A big benefits of DDS: It is truly distributed

- It has no central server
- This makes it robust;
- There is no single-point-failure

A drawback of DDS: it has a steep learning curve

- It has many, advances features
- It is simple to use; after one grasp the concepts
- These DDS-Demonstrators will help to understand & use DDS


The “DDS-stack” is independent of the platform (hardware & OS), the used programming language, and the supplier! Unlike many other protocol, both the “wire-protocol” and the “API” are standardized. This should make it possible to “plug and play” several implementations - at least on paper. Part of this program is to play and experiment with those promises; as well as with other features [#PnP]_.

DDS is implemented by several suppliers and comes in multiple packages. There are also some ‘open’ or ‘community’ editions. Given the goal of the DDS-Demonstrators, we will restrict ourselves to open-source implementations.(e.g. `CycloneDDS <https://github.com/eclipse-cyclonedds/cyclonedds>`_ and `eProsima Fast RTPS <https://github.com/eProsima/Fast-RTPS>`_).
|BR|
We focus on `CycloneDDS`, an `Eclipse-Foundation <https://projects.eclipse.org/projects/iot.cyclonedds>`_ project [#EFI]_. And will add `eProsima Fast RTPS`, as we expect it may be smaller, more suitable for really small embedded systems. Again, we have to experiment with that [#ROS]_.


More info
=========
* Wikipedia: https://en.wikipedia.org/wiki/Data_Distribution_Service
* OMG (*the* standards) https://www.omg.org/omg-dds-portal/
* DDS-Foundation: https://www.dds-foundation.org


.. rubric:: Footnotes

.. [#PnP]  We already have some indications this is different in practice. By example: a least one (working)
           python-binding is only able to talks to nodes with the same python-binding. More on that later.
.. [#EFI]  Which is a quite trivial selection method. We also like it has Dutch roots (like DDS itself).
.. [#ROS]  We also like `eProsima Fast RTPS` as it be used in ROS (Robot Operating System); which we also use.
