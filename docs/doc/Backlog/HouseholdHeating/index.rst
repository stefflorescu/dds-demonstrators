.. (C) ALbert Mietus, Sogeti, 2019

==============================
Household Heating Demonstrator
==============================

.. warning:: On Hold

   This Demonstrator is on-hold for now; it has a technical complexity that was initially not foreseen. And was not part of the project.

   As this control-loop has a feedback loop, a simple “event driven” architecture can/will result in (indirect)
   self-trigering of all nodes. That is not indented! This is easy to solve using “controll-loop concepts”; but that is
   not the goal of this Demonstrator.

.. uml::

      title “Household Heating”
      !include ../../_inc/class-as-Node.iuml

      class “Clock/\nProgram” as clock
      class “Controller\nPID” as pid
      class “Heater\n(actor)” as actor
      class “House\n(model)”  as house
      class “Temp\n(sensor)”  as sensor

      clock -right-|> pid: set_temp

      pid   -right-|>  actor:  percentage
      actor  -down-|>  house:  on/off
      house  -left-|>  sensor: heat
      sensor   -up-|>  pid:    actual_temp


.. demo:: Household Heating
   :ID: HH_001
   :style: needs_red
   :tags: HH
   :status: on-hold

   To Be Done

