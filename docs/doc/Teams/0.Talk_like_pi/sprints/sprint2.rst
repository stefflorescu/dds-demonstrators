sprint2
=======

The first sprint has passed its time for the second sprint.


sprint results 1
----------------
   Using the method of sending a whole bunch of message at the same time and timing how long it takes before they get back the following
   milliseconds per message have been measured using the protocols isc capabilities( dont understand what is actually being measured? look at the bandwidth chapter in :doc:`../RoundTrip`).

   +-----------------------+-----------------------+
   | ISC measurements bandwidth                    |
   +-----------------------+-----------------------+
   | Language:               python3               |
   +-----------------------+-----------------------+
   | protocol              |  ms/msg               |
   +-----------------------+-----------------------+
   | MQTT                  |  6.5                  |
   +-----------------------+-----------------------+
   | ZMQ                   |  7.3                  |
   +-----------------------+-----------------------+
   | DDS                   | under construction    |
   +-----------------------+-----------------------+

   If the processes communicate in an interprocess way the following times are measured:

   +-----------------------+-----------------------+
   | IPC measurements bandwidth                    |
   +-----------------------+-----------------------+
   | Language:               python3               |
   +-----------------------+-----------------------+
   | protocol              |  ms/msg               |
   +-----------------------+-----------------------+
   | MQTT                  |  9.5                  |
   +-----------------------+-----------------------+
   | ZMQ                   |  0.4                  |
   +-----------------------+-----------------------+
   | DDS                   | under construction    |
   +-----------------------+-----------------------+

sprint features
---------------

Total amount of feature points to spend: 40

Below all options with their tasks have been laid out that can be taken up for this sprint.

sprint options:
   * Compare 10 ZMQ variations in python **[15 fp]** (hp)
      - research ten possible configurations for ZMQ **[2 fp]**
      - first ping pong **[2 fp]**
      - first round trip **[2 fp]**
      - second ping pong **[1 fp]**
      - second round trip **[1 fp]**
      - next added ping pong **[0.5 fp]**
      - next added roud trip **[0.5 fp]**
   * Compare 3 different MQTT setups in python **[2 fp]** (hp)
      - research three possible MQTT setups **[1 fp]**
      - measuring round trip time **[1 fp]**
   * Change DDS to cyclonedds from eclipse and timing ISC **[2 fp]** (hp)
      - install cyclonedds **[1 fp]**
      - test cyclonedds **[1 fp]**
   * Round trip time MQTT in C++ IPC and ISC **[4 fp]**
      - install library **[1 fp]**
      - working pingpong **[2 fp]**
      - round trip time  **[1 fp]**
        This ^ is the same for each of the following round trip time

   * Round trip time DDS in C++ IPC and ISC **[4 fp]**
   * Round trip time ZMQ in C++ IPC and ISC **[4 fp]**
   * Round trip time MQTT in java IPC and ISC **[4 fp]**
   * Round trip time DDS in java IPC and ISC **[4 fp]**
   * Round trip time ZMQ in java IPC and ISC **[4 fp]**
   * bandwidth measurement 2 fp per language and protocol **[18 fp]** 
      - write program that measures bandwidth **[2 fp]**
   * Documentation for sprint 1 **[4 fp]**

      - planned features and finished futures **[2 fp]**
      - sprint results in sphinx table format **[2 fp]**

   * Documentation for sprint 2 **[8 fp]**
      - sprint results **[2 fp]**
      - software installation and execution **[4 fp]**
      - ZMQ, MQTT and DDS workings **[2 fp]**

   * Pathways setup and protocol integration **[24 fp]**
   
chosen sprint goals:
   * Compare 10 ZMQ variations in python **[15 fp]** (hp)
   * Compare 3 different MQTT setups in python **[2 fp]** (hp)
   * Change DDS to cyclonedds from eclipse and timing ISC **[2 fp]** (hp)
   * Documentation for sprint 2 **[8 fp]** (hp)
   * Documentation for sprint 1 **[4 fp]** (mp)
   * banddiwdth measures for python ZMQ, MQTT and DDS **[6 fp]** (mp)
   * Round trip time DDS in C++ IPC and ISC **[4 fp]** (lp)
   * Round trip time ZMQ in C++ IPC and ISC **[4 fp]** (lp)
  
   






 
