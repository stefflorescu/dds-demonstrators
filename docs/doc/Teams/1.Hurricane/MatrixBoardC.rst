.. _MatrixBoardC:

Running the Matrix Board Communication in C
-------------------------------------------

Before following this guide in order to execute the matrix board communication make sure you have successfully installed CycloneDDS (:ref:`setupCycloneDDS`).  

Building the executable
"""""""""""""""""""""""

Make sure your local repo is up to date with the `HighTech-nl repo <https://bitbucket.org/HighTech-nl/dds-demonstrators/src/master/>`_ and move into the directory ``/dds-demonstrators/src/demonstrators/MatrixBoard/C``::

   $ mkdir build
   $ cd build 
   $ cmake .. 
   $ cmake --build .


Running the Matrix Board Communication
""""""""""""""""""""""""""""""""""""""

The demonstrator can be executed either on one machine by creating different processes (in different terminal windows) 
or by creating instances of the matrix boards on different nodes (i.e. on Raspberry Pis). A combination of the two methods can also be utilized, in other words 
running multiple processes on each of the nodes in the network. 

In order to run a matrix board instance execute the following command by providing an integer number which is the ID of that particular board (i.e. a positive number)::

    $ ./MBC <MBC_ID>

.. note:: 

   In the case that the wrong network interface is choosen by DDS at startup of the application see the notes in :ref:`setupCycloneDDS` for details on how to utilize the desired network interface.

.. note:: 

   See :ref:`rpis` for a complete guide on Raspberry Pi networking as well as the IP addresses for the network that is already in place. 


      


